import { LineString } from 'ol/geom';
import { MIN_NUM_OF_COORDINATES } from '../config/constans';

export const checkFeatureForPolygon = feature => {
  const featureCoordinatesCount = feature.getGeometry().getFlatCoordinates().length;

  if (featureCoordinatesCount < MIN_NUM_OF_COORDINATES) {
    return false;
  }

  const firstCoordinate = feature.getGeometry().getFirstCoordinate();
  const lastCoordinate = feature.getGeometry().getLastCoordinate();
  const distance = new LineString([firstCoordinate, lastCoordinate]).getLength().toFixed(2);

  return (distance < 200);
};
