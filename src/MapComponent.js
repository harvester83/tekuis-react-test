import React from 'react';
import Feature from 'ol/Feature';
import { LineString, Polygon } from 'ol/geom';
import Collection from 'ol/Collection';
import { createLayer } from './modules/createLayer';
import { checkFeatureForPolygon } from './modules/checkFeatureForPolygon';
import { initMap } from './modules/initMap';

class MapComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isAllowToDraw: false,
      mapDomRef: React.createRef(),
      features: new Collection(),
      currentFeature: null,
      feachureCoordinates: [],
      newSegmentCoordinates: [],
    };
  }

  componentDidMount() {
    const layers = new Collection();
    const map = initMap(layers, this.state.mapDomRef.current);

    layers.push(createLayer(this.state.features));

    map.on('click', this.drawLine);
    map.on('pointermove', this.drawTempLine);
    map.on('dblclick', this.saveLine);
  }

  drawLine = event => {
    if (! this.state.isAllowToDraw) {
      return;
    }

    let newSegmentCoordinates = event.coordinate;

    if (this.state.newSegmentCoordinates.length !== 0) {
        newSegmentCoordinates = this.state.newSegmentCoordinates;
    }

    this.state.feachureCoordinates.push(newSegmentCoordinates);
  };

  drawTempLine = event => {
    if (! this.state.isAllowToDraw) {
      return;
    }

    let currentFeature = this.state.currentFeature;
    if (currentFeature) {
      this.state.features.remove(currentFeature);
    }

    let newSegmentCoordinates = this.calculatePerpendicularCoordinates(event);
    this.state.newSegmentCoordinates = newSegmentCoordinates;

    let coordinates = [...this.state.feachureCoordinates, newSegmentCoordinates];
    let lineString = new LineString(coordinates);

    this.state.currentFeature = new Feature(lineString);
    this.state.features.push(this.state.currentFeature);

    console.log('features.getLength', this.state.features.getLength());
  };

  calculatePerpendicularCoordinates = event => {
    let features = [ ...this.state.feachureCoordinates ];
    features = features.reverse();
    let mouseCoordinate = event.coordinate || [0, 0];

    if (! features[0]) {
      return mouseCoordinate;
    }

    let [x2, y2] = features[0];

    if (! features[1]) {
      return mouseCoordinate;
    }

    let [x1, y1] = features[1];
    let [, y3] = mouseCoordinate;
    let vectorAx = x2-x1;
    let vectorAy = y2-y1;
    let vectorBy = y3-y2;

    let x3 = x2 - (vectorAy * vectorBy) / vectorAx;

    return  [x3, y3];
  };

  saveLine = event => {
    if (!this.state.isAllowToDraw) {
      return;
    }

    event.preventDefault();

    if (checkFeatureForPolygon(this.state.currentFeature)) {
      this.state.features.remove(this.state.currentFeature);
      this.state.features.push(new Feature(new Polygon([this.state.feachureCoordinates])));
    }

    this.state.feachureCoordinates = [];
    this.state.currentFeature = null;
  };

  toggleToolsDraw = status => {
    this.setState({
      isAllowToDraw: status,
    });
  };

  render() {
    return (
      <div className="App">
        <div className={`map ${this.state.isAllowToDraw && 'crosshair'}`} ref={this.state.mapDomRef}></div>

        <div className="mt-2">
          <button
            className={`btn btn-primary mr-2 ${this.state.isAllowToDraw && 'btn-primary-active'}`}
            onClick={() => this.toggleToolsDraw(true)}
          >
            MEASURETOOLS
          </button>

          <button
            className={`btn btn-primary mr-2 ${this.state.isAllowToDraw || 'btn-primary-active'}`}
            onClick={() => this.toggleToolsDraw(false)}
          >
            STOP
          </button>
        </div>
      </div>
    );
  }
}

export default MapComponent;
