import React from 'react';
import ReactDOM from 'react-dom';
import MapComponent from './MapComponent';
import './index.css';

const rootElement = document.getElementById('root');
ReactDOM.render(<MapComponent />, rootElement);
