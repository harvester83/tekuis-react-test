import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import { Fill, Stroke, Style, Text } from 'ol/style';
import { LineString, Polygon } from 'ol/geom';
import settings from '../config/settings';

export const createLayer = features => {
  return new VectorLayer({
    source: new VectorSource({
      features: features,
    }),
    style: feature => {
      const featureGeometry = feature.getGeometry();
      let measurements = 0;

      if (featureGeometry instanceof LineString) {
        measurements =
          feature
            .getGeometry()
            .getLength()
            .toFixed(2)
            .toString() + ' m';
      }

      if (featureGeometry instanceof Polygon) {
        measurements =
          feature
            .getGeometry()
            .getArea()
            .toFixed(2)
            .toString() + ' m2';
      }

      return new Style({
        stroke: new Stroke({
          width: settings.lineWidth,
          color: settings.lineColor,
        }),
        fill: new Fill({
          color: settings.lineFillColor,
        }),
        text: new Text({
          offsetY: settings.textOffsetY,
          placement: 'line',
          scale: settings.textScale,
          text: measurements,
          fill: new Fill({
            color: settings.textFillColor,
          }),
          stroke: new Stroke({
            width: settings.textWidth,
            color: settings.textColor,
          }),
        }),
      });
    },
  });
};
