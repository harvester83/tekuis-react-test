import TileLayer from 'ol/layer/Tile';
import OSM from 'ol/source/OSM';
import Map from 'ol/Map';
import View from 'ol/View';
import { fromLonLat } from 'ol/proj';

export const initMap = (layers, target) => {
  const rasterLayer = new TileLayer({
    source: new OSM(),
  });

  layers.push(rasterLayer);

  return new Map({
    target: target,
    layers: layers,
    view: new View({
      center: fromLonLat([12.5, 41.9]),
      zoom: 18,
    }),
  });
};
